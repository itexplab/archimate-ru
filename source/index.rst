.. ArchiMate documentation master file, created by
   sphinx-quickstart on Mon Dec 13 14:16:02 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ArchiMate — русскоязычная документация
======================================

Русскоязычный перевод `документации <https://pubs.opengroup.org/architecture/archimate3-doc/>`_ по языку моделирования архитектуры предприятия ArchiMate.

`Archi <https://www.archimatetool.com/>`_ — инструмент моделирования с открытым исходным кодом для создания архитектурных моделей предприятия.

Русскоязычное сообщество пользователей ArchiMate в Telegram: https://t.me/archimate_ru

.. note:: Последняя правка |today|
    

.. toctree::
   :caption: Оглавление
   :maxdepth: 1
   :titlesonly:
   
   preface
   
.. toctree::
   :maxdepth: 1
   :numbered:
   
   preface  
   


.. Indices and tables
   ==================
   
..  * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
