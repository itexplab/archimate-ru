===========
Предисловие
===========


The Open Group
---------------------

The Open Group — это глобальный консорциум, который обеспечивает достижение бизнес-целей с помощью технологических стандартов. Наше разнообразное членство в более чем 700 организациях включает заказчиков, поставщиков систем и решений, поставщиков инструментов, интеграторов, ученых и консультантов из различных отраслей промышленности.

Миссия The Open Group состоит в том, чтобы способствовать созданию Безграничного информационного потока (Boundaryless Information Flow™), достигаемого за счет:

* Работы с клиентами для выявления, понимания и удовлетворения текущих и возникающих требований, разработки политики и обмена передовым опытом.

* Работы с поставщиками, консорциумами и органами по стандартизации для достижения консенсуса и обеспечения совместимости, разработки и интеграции спецификаций и технологий с открытым исходным кодом

* Предоставления комплексного набора услуг для повышения операционной эффективности консорциумов

* Разработки и эксплуатации ведущей в отрасли службы сертификации и стимулирование закупок сертифицированной продукции

Более подробная информация о The Open Group доступна по адресу `www.opengroup.org <http://www.opengroup.org/>`_.

The Open Group публикует широкий спектр технической документации, большая часть которой посвящена разработке стандартов и руководств The Open Group, но также включает в себя официальные документы, технические исследования, документацию по сертификации и тестированию, а также деловые издания. Полная информация и каталог доступны по адресу `www.opengroup.org/library <http://www.opengroup.org/library>`_.


О документе
---------------------

Данный документ является спецификацией ArchiMate® 3.1, стандарта The Open Group. Стандарт был разработан и одобрен консорциумом The Open Group.

Настоящее издание стандарта включает ряд исправлений, уточнений и улучшений по сравнению с предыдущим изданием, а также несколько дополнений.


Целевая аудитория
---------------------


Целевая аудитория данного стандарта состоит из трех групп:

* Все те, кто работает над формированием и внедрением сложных организационных изменений.

  Типичные названия должностей включают специалистов по корпоративной архитектуре, бизнес-архитекторов, ИТ-архитекторов, архитекторов приложений, архитекторов данных, информационных архитекторов, архитекторов процессов, архитекторов инфраструктуры, архитекторов программного обеспечения, системных архитекторов, архитекторов решений, менеджеров по продуктам/услугам, высшее и оперативное руководство, руководители проектов и все, кто работает в рамках эталонной структуры, определенной архитектурой предприятия.

* Те, кто намерен реализовать язык ArchiMate в программном средстве.

  Они найдут полное и подробное описание языка в этом документе.

* Академическое сообщество, на которое мы опираемся при внесении изменений и улучшении формулировок на основе новейших исследований в области архитектуры.


Структура
---------------------

Структура документа включает в себя:

* Глава 1. Введение — содержит введение в стандарт, включая цели, краткий обзор, требования к соответствию и терминологию.
* Глава 2. Определения — определяет общие термины, используемые в настоящем стандарте
* Глава 3. Структура языка — описывает структуру языка моделирования ArchiMate, включая структуру верхнего уровня, многослойность, базовую структуру ArchiMate и полную структуру ArchiMate.
* Глава 4. Общая метамодель — описывает структуру и элементы общей метамодели ArchiMate
* Глава 5. Отношения — описывает отношения в языке
* Глава 6. Элементы мотивации — описывает концепции выражения мотивации для архитектуры вместе с примерами
* Глава 7. Элементы стратегии — содержит элементы для моделирования предприятия на стратегическом уровне вместе с примерами
* Глава 8. Бизнес-уровень — охватывает определение и использование элементов бизнес-уровня вместе с примерами
* Глава 9. Прикладной уровень — охватывает определение и использование элементов прикладного уровня вместе с примерами
* Глава 10. Технологический уровень — охватывает определение и использование элементов технологического уровня вместе с примерами
* Глава 11. Физические элементы — описывает языковые элементы для моделирования физического мира вместе с примерами
* Глава 12. Отношения между основными уровнями — охватывает отношения между различными уровнями языка.
* Глава 13. Элементы внедрения и миграции — описывает языковые элементы для выражения аспектов внедрения и миграции архитектуры (например, проекты, программы, плато и пробелы).
* Глава 14. Заинтересованные стороны, взгляды на архитектуру и точки зрения — описывает механизм точки зрения ArchiMate.
* Глава 15. Механизмы настройки языка — описывается, как настроить язык ArchiMate для специализированных или специфичных для домена целей.
* Приложение А. Краткое описание языковой нотации — является информационным приложением.
* Приложение B. Отношения — является нормативным приложением, в котором подробно описываются требуемые отношения между элементами языка и правилами для их получения.
* Приложение C. Примеры точек зрения — представляет набор точек зрения архитектуры, разработанных в нотации ArchiMate на основе практического опыта

  Все точки зрения подробно описаны. В приложении указаны элементы, взаимосвязи, рекомендации по использованию, цели и целевые группы для каждой точки зрения.

* Приложение D. Связь с другими стандартами, спецификациями и руководящими документами — описывает связь языка ArchiMate с другими стандартами и спецификациями, включая платформу TOGAF®, руководство BIZBOK®, BPMN™ , UML® и BMM™.
* Приложение E. Изменения с версии 2.1 до версии 3.1, представляет собой информационное приложение, в котором излагаются изменения в стандарте между версиями 2.1 и 3.1.


Товарные знаки
---------------------

ArchiMate®, DirecNet®, Making Standards Work®, логотип Open O®, Open O и логотип сертификации Check®, OpenPegasus®, Platform 3.0®, The Open Group®, TOGAF®, UNIX®, UNIXWARE®, логотип Open Brand X® и зарегистрированные торговые знаки Agile Architecture Framework™, Boundaryless Information Flow™, Build with Integrity Buy with Confidence™, Dependability Through Assuredness™, Digital Practitioner Body of Knowledge™, DPBoK™, EMMM™, FACE™, логотип FACE™, IT4IT™, логотип IT4IT™, O-AAF™, O-DEF™, O-HERA™, O-PAS™, Open FAIR™, Open Platform 3.0™, Open Process Automation™, Open Subsurface Data Universe™, Open Trusted Technology Provider™, O-SDU™, Sensor Integration Simplified™, SOSA™ и логотип SOSA™ — зарегистрированные торговые знаки The Open Group.

Guide to the Business Architecture Body of Knowledge® и BIZBOK® — зарегистрированные торговые знаки Business Architecture Guild.

Java® — зарегистрированный торговый знак Oracle и/или родственных организаций.

UML® и Unified Modeling Language®, зарегистрированные торговые знаки BMM™, BPMN™, Business Motivation Model™, и Business Process Modeling Notation™ — зарегистрированные торговые знаки Object Management Group.

Все остальные бренды, названия компаний и продуктов используются только в целях идентификации и могут быть товарными знаками, являющимися исключительной собственностью их соответствующих владельцев.


Благодарности
---------------------

The Open Group выражает благодарность The Open Group ArchiMate Forum за разработку этого стандарта.

The Open Group выражает благодарность следующим людям за вклад в разработку этой и более ранних версий данного стандарта:

* Iver Band, EA Principals & Cambia Health Solutions
* Thorbjørn Ellefsen, Capgemini
* William Estrem, Metaplexity Associates
* Maria-Eugenia Iacob, University of Twente
* Henk Jonkers, BiZZdesign
* Marc M. Lankhorst, BiZZdesign
* Dag Nilsen, Biner
* Carlo Poli, Macaw
* Erik (H.A.) Proper, Luxembourg Institute for Science and Technology & Radboud University Nijmegen
* Dick A.C. Quartel, BiZZdesign
* G. Edward Roberts, Elparazim
* Jean-Baptiste Sarrodie, Accenture
* Serge Thorn, Metaplexity Fellow

Команда проекта The Open Group и ArchiMate также хотела бы поблагодарить следующих лиц за их поддержку и экспертную оценку этой и более ранних версий данного стандарта:

* Adina Aldea
* Mary Beijleveld
* Alexander Bielowski
* Remco de Boer
* Adrian Campbell
* John Coleshaw
* Jörgen Dahlberg
* Garry Doherty
* Ingvar Elmér
* Wilco Engelsman
* Roland Ettema
* Henry M. Franken
* Mats Gejnevall
* Sonia González
* Kirk Hansen
* Jos van Hillegersberg
* Andrew Josey
* Ryan Kennedy
* Louw Labuschagne
* Antoine Lonjon
* Veer Muchandi
* Michelle Nieuwoudt
* Erwin Oord
* Antonio Plais, Centus
* Daniel Simon
* Gerben Wierda
* Egon Willemsz

Первая версия данного стандарта была в значительной степени подготовлена проектом ArchiMate. The Open Group с благодарностью признает вклад многих людей — бывших членов проектной команды, которые внесли в нее свой вклад.

Проект ArchiMate включал в себя следующие организации:

* ABN AMRO
* Centrum voor Wiskunde en Informatica
* Dutch Tax and Customs Administration
* Leiden Institute of Advanced Computer Science
* Novay
* Ordina
* Radboud Universiteit Nijmegen
* Stichting Pensioenfonds ABP


Справочные материалы
---------------------

Настоящий стандарт содержит ссылки на следующие документы. Эти ссылки носят информационный характер.

.. note:: Пожалуйста, обратите внимание, что ссылки ниже действительны на момент написания статьи, однако их доступность не может быть гарантирована в будущем.

[1] Enterprise Architecture at Work: Modeling, Communication, and Analysis, Third Edition, M.M. Lankhorst et al., Springer, 2013.

[2] The Anatomy of the ArchiMate® Language, M.M. Lankhorst, H.A. Proper, H. Jonkers, International Journal of Information Systems Modeling and Design (IJISMD), 1(1):1-32, January-March 2010.

[3] Extending Enterprise Architecture Modeling with Business Goals and Requirements, W. Engelsman, D.A.C. Quartel, H. Jonkers, M.J. van Sinderen, Enterprise Information Systems, 5(1):9-36, 2011.

[4] TOGAF® Version 9.2, The Open Group Standard (C182), April 2018, published by The Open Group; refer to: `www.opengroup.org/library/c182 <http://www.opengroup.org/library/c182>`_.

[5] Extending and Formalizing the Framework for Information Systems Architecture, J.F. Sowa, J.A. Zachman, IBM Systems Journal, Volume 31, No. 3, pp.590-616, 1992.

[6] TOGAF® Framework and ArchiMate® Modeling Language Harmonization: A Practitioner’s Guide to Using the TOGAF® Framework and the ArchiMate® Language, White Paper (W14C), December 2014, published by The Open Group; refer to: `www.opengroup.org/library/w14c <http://www.opengroup.org/library/w14c>`_.

[7] Unified Modeling Language®: Superstructure, Version 2.0 (formal/05-07-04), Object Management Group, August 2005.

[8] Unified Modeling Language®: Infrastructure, Version 2.4.1 (formal/201-08-05), Object Management Group, August 2011.

[9] A Business Process Design Language, H. Eertink, W. Janssen, P. Oude Luttighuis, W. Teeuw, C. Vissers, in Proceedings of the First World Congress on Formal Methods, Toulouse, France, September 1999.

[10] Enterprise Business Architecture: The Formal Link Between Strategy and Results, R. Whittle, C.B. Myrick, CRC Press, 2004.

[11] Composition of Relations in Enterprise Architecture, R. van Buuren, H. Jonkers, M.E. Iacob, P. Strating, in Proceedings of the Second International Conference on Graph Transformation, pp.39-53, edited by H. Ehrig et al., Rome, Italy, 2004.

[12] Business Process Modeling Notation™ (BPMN™), Version 2.0 (formal/2011-01-03), Object Management Group, 2011.

[13] Performance and Cost Analysis of Service-Oriented Enterprise Architectures, H. Jonkers, M.E. Iacob, in Global Implications of Modern Enterprise Information Systems: Technologies and Applications, edited by A. Gunasekaran, IGI Global, 2009.

[14] ISO/IEC 42010:2011, Systems and Software Engineering – Recommended Practice for Architectural Description of Software-Intensive Systems, Edition 1.

[15] Business Motivation Model™ (BMM™), Version 1.1 (formal/2010-05-01), Object Management Group, 2010.

[16] Using the ArchiMate® Language with UML®, White Paper (W134), September 2013, published by The Open Group; refer to: `www.opengroup.org/library/w134 <http://www.opengroup.org/library/w134>`_.

[17] TOGAF® Series Guide: Value Streams (G178), October 2017, published by The Open Group: refer to: `www.opengroup.org/library/g178 <https://publications.opengroup.org/g170>`_.

[18] Business Architecture Guild. A Guide to the Business Architecture Body of Knowledge® (BIZBOK® Guide), Version 7.0, 2018; refer to: `www.businessarchitectureguild.org <https://www.businessarchitectureguild.org/default.aspx>`_.

[19] TOGAF® Series Guide: The TOGAF® Technical Reference Model (TRM) (G175), September 2017, published by The Open Group: refer to: `www.opengroup.org/library/g175 <https://publications.opengroup.org/g175>`_.

[20] ArchiMate® Model Exchange File Format for the ArchiMate Modeling Language, Version 3.0, The Open Group Standard (C174), May 2017, published by The Open Group; refer to: `www.opengroup.org/library/c174 <https://publications.opengroup.org/c174>`_.