# archimate-ru

## ArchiMate — русскоязычная документация

Русскоязычный перевод [документации](https://pubs.opengroup.org/architecture/archimate3-doc/) по языку моделирования архитектуры предприятия ArchiMate.

[Archi](https://www.archimatetool.com/) — инструмент моделирования с открытым исходным кодом для создания архитектурных моделей предприятия.

Русскоязычное сообщество пользователей ArchiMate в Telegram: https://t.me/archimate_ru